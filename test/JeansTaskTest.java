import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JeansTaskTest {

    @Test
    public void checkDiscountPrice() {
        double price = 1000;
        double discount = 60;

        assertEquals(400, JeansTask.getDiscountPrice(price, discount));
    }

    @Test
    public void checkWrongDiscountPrice() {
        double price = 1000;
        double wrongDiscount = 95;

        //
    }
}