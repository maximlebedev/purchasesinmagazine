import java.util.Scanner;

public class JeansTask {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Введите сколько у вас денег: ");
            double money = scanner.nextDouble();

            System.out.print("Введите цену на джины: ");
            double price = scanner.nextDouble();

            System.out.print("Введите скидку на джинсы: ");
            double discount = scanner.nextDouble();

            money = makePurchase(price, discount, money);

            if (money >= 0) {
                System.out.println("Вы купили джинсы, у вас осталось: " + money);
            } else {
                System.out.println("У вас не хватило денег!");
            }
        } catch (Exception e) {
            System.out.println("Упс, что-то введено не верно!");
        }
    }

    public static double getDiscountPrice(double price, double discount) {
        double discountPrice = price - (price / 100) * discount;

        return discountPrice;
    }

    public static double makePurchase(double price, double discount, double money) {
        double discountPrice = getDiscountPrice(price, discount);

        if (money - discountPrice >= 0) {
            money -= discountPrice;
        } else
            money = -1;


        return money;
    }
}